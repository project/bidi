BiDi Theme README
------------------

This is a bidi (bi-directional) theme for Drupal, based on box_grey, it uses the PHPTemplate engine.
The theme can be used for right-to-left locale sites (like Arabic), or multi-lingual sites with one or more enabled RTL (Right to Left) locales.

Installation
-------------
1) Copy BiDi's folder to your themes folder.
2) Copy files under bidi/misc to Drupal's misc folder.
3) Enable theme in admin/themes page.
4) Enable locale module in admin/modules.
5) Enable a right-to-left locale in admin/localization.

Known Issues
-------------
* Node comments are always padded to left, to fix this comment and forum modules need to be patched.
* Print-friendly book pages are aligned to left, to fix this book module needs to be patched.

TODO
-----
* Create patches for mentioned issues.

Author
-------
Ayman Hourieh
E-mail: drupal@aymanh.com
WWW: http://aymanh.com/

Based on box_grey
------------------
adrinux
mailto: adrinux@gmail.com
IM: perlucida
